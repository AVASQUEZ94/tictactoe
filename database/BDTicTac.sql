-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-10-2018 a las 00:19:23
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gametictac`
--
DROP DATABASE IF EXISTS `gametictac`;
CREATE DATABASE IF NOT EXISTS `gametictac` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `gametictac`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `board`
--

CREATE TABLE `board` (
  `id` int(11) NOT NULL,
  `id_match` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `value` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `board`
--

INSERT INTO `board` (`id`, `id_match`, `position`, `value`, `created_at`, `updated_at`) VALUES
(1, 3, 0, 2, '2018-10-17 21:58:06', '2018-10-18 00:58:06'),
(2, 3, 1, 2, '2018-10-17 21:58:18', '2018-10-18 00:58:18'),
(3, 3, 2, 2, '2018-10-17 21:58:16', '2018-10-18 00:58:16'),
(4, 3, 3, 2, '2018-10-17 21:58:21', '2018-10-18 00:58:21'),
(5, 3, 4, 2, '2018-10-17 21:58:09', '2018-10-18 00:58:09'),
(6, 3, 5, 2, '2018-10-17 21:58:19', '2018-10-18 00:58:19'),
(7, 3, 6, 2, '2018-10-17 21:58:14', '2018-10-18 00:58:14'),
(8, 3, 7, 2, '2018-10-17 21:58:20', '2018-10-18 00:58:20'),
(9, 3, 8, 2, '2018-10-17 21:58:12', '2018-10-18 00:58:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matches`
--

CREATE TABLE `matches` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `next` int(11) NOT NULL DEFAULT '1',
  `winner` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `matches`
--

INSERT INTO `matches` (`id`, `name`, `next`, `winner`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Juego de X-O', 1, 0, 1, '2018-10-17 21:30:56', '0000-00-00 00:00:00'),
(6, 'Juegoooo X', 1, 0, 1, '2018-10-18 01:11:10', '2018-10-18 01:11:10'),
(7, 'Juegoooo X', 1, 0, 1, '2018-10-18 01:11:28', '2018-10-18 01:11:28'),
(8, 'Juegoooo X', 1, 0, 1, '2018-10-18 01:13:01', '2018-10-18 01:13:01'),
(9, 'Juegoooo X', 1, 0, 1, '2018-10-18 01:13:17', '2018-10-18 01:13:17');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `board`
--
ALTER TABLE `board`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_match` (`id_match`);

--
-- Indices de la tabla `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `board`
--
ALTER TABLE `board`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `matches`
--
ALTER TABLE `matches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `board`
--
ALTER TABLE `board`
  ADD CONSTRAINT `board_ibfk_1` FOREIGN KEY (`id_match`) REFERENCES `matches` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
