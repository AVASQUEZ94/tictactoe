<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Matches; //vincule with match's model
use App\Boards;

class MatchController extends Controller {

    public function index() {
         return view('index');
    }

    /**
     * Returns a list of matches
     *
     * TODO it's mocked, make this work :)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function matches() {
        return response()->json($this->fakeMatches());
    }

    /**
     * Returns the state of a single match
     *
     * TODO it's mocked, make this work :)
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function match($id) {
		
	 $match=Matches::find($id);
	 
	   $board = $match->getArrayBoard($match);
	   
        return response()->json([
             'id' => $id,
            'name' => $match->name.' - ID: '.$id,
            'next' => $match->next,
            'winner' => $match->winner,
            'board' => $board,
        ]);
    }
 
    /**
     * Makes a move in a match
     *
     * TODO it's mocked, make this work :)
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function move($id) {
		
		$match=Matches::find($id);
		$board = $match->getArrayBoard($match);
		
        $position = Input::get('position');
		
        $board[$position] = 2;
		
		$match->saveMovement($position,$match);
		
        return response()->json([
             'id' => $id,
            'name' => $match->name.' - ID: '.$id,
            'next' => $match->next,
            'winner' => $match->winner,
            'board' => $board,
        ]);
    }

    /**
     * Creates a new match and returns the new list of matches
     *
     * TODO it's mocked, make this work :)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request) {
		
		//save a new match
		     $match = new Matches;
			 $match->name = $request->name;
			 $match->save();
			 
		//return all match's
        return response()->json($this->fakeMatches());
    }

    /**
     * Deletes the match and returns the new list of matches
     *
     * TODO it's mocked, make this work :)
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id) {
		
		Matches::destroy($id); //delete permanently or we can use softdelete. with value 0
		
        return response()->json($this->fakeMatches()->filter(function($match) use($id){
            return $match['id'] != $id;
        })->values());
			
    }

    /**
     * Creates a fake array of matches
     *
     * @return \Illuminate\Support\Collection
     */
    private function fakeMatches() {
				
		// get all match's in bd
		$matches=Matches::all()->where("status",1);
		
		return	$matches;	
  
    }

}