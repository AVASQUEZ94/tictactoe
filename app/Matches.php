<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matches extends Model
{
    //table name
	  protected $table = 'matches';
	
	//relation with table "board"
	public function board(){
        return $this->hasMany('App\Boards','id_match');
    }
	
	//helper to convert in array
	public function getArrayBoard($table){
		$pos=0;
		$board=null; 
		
			foreach ($table->board as $key){
				$board[$pos]=$key->value; 
				$pos++;			
			}
		
		return $board;
		
	}
	
	//save movement
	public function saveMovement($position,$match){
		
		foreach ($match->board as $key) {
		
			if ($key->position==$position){
				$key->value = 2;
				$key->save();
			}
		}
		
	}
	
	//save movement
	public function saveNewGamePositions($match,$id){
					
		
	}
	

}
