<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boards extends Model
{
    //table name
	 protected $table = 'board';
	 
	//relation with table match's
	public function match(){
			return $this->belongsTo('App\Matches');
		}
	 
}
